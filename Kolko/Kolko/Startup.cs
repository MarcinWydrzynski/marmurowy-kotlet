﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kolko.Startup))]
namespace Kolko
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
